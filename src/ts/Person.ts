import { PersonInterface } from './interfaces/PersonInterface';

export class Person {
  person: PersonInterface = {
    name: '',
    age: 0
  };

  constructor(name: string, age: number) {
    this.person.name = name;
    this.person.age = age;
  }

  getName(): string {
    return this.person.name;
  }

  getAge(): number {
    return this.person.age;
  }

  setName(name: string): void {
    this.person.name = name;
  }

  setAge(age: number): void {
    this.person.age = age;
  }

  getPerson(): PersonInterface {
    return this.person;
  }
}