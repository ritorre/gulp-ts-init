import { Person } from './Person';

function showHello(divName: string, name: string) {
    const elt = document.getElementById(divName);
    let person = new Person('Typescript', 5);
    elt.innerText = `Hello ${person.getName()} your age is ${person.getAge()}`;
}

showHello('greeting', 'Typescript');