# gulp-ts-init

Starter kit with Typescript and gulp. Gulp is configured to compile Typescript in minified Javascript code.

Run 
<code>
  npm bild / yarn build
</code>

this command generates the dist folder, where the compiled code is.

Run
<code>
  npm start / yarn start
</code>

to see the application running in the browser